(ns advent.day8)

(defn input [width height]
  (->> "src/day8.raw" (slurp) (clojure.string/trim) (seq)))

(let [width 25 height 6]
  (->> (input width height)
       (partition (* width height)) (reverse)
       (reduce (fn [under over] (map #(if (= %2 \2) %1 %2) under over)))
       (partition width) (map println)))
