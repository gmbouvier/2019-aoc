(ns advent.day2
  (:require [clojure.java.io :as io]))

(def input
  (into [] (map (fn [i] (Integer/parseInt i)))
        (clojure.string/split
         (-> "day2.raw"
             (io/resource)
             (io/reader)
             (line-seq)
             (first))
         #",")))

(defn perform-op [op arg1 arg2]
  (cond
    (= 1 op) (+ arg1 arg2)
    (= 2 op) (* arg1 arg2)
    (= 99 op) :done))

(defn do-prog [pointer line]
  (let [try-nth #(try (nth line %) (catch Exception e nil))
        arg #(->> % (+ pointer) (try-nth) (try-nth))
        result (perform-op (nth line pointer) (arg 1) (arg 2))]
    (if (= result :done) line
        (recur (+ 4 pointer)
               (assoc line (try-nth (+ pointer 3)) result)))))

(defn try-input [one two]
  (do-prog 0 (assoc (assoc input 1 one) 2 two)))

(defn part2 []
  (let [range-99 (into [] (range 99))
        attempts
        (for [a range-99 b range-99]
          (-> (try-input a b)
              (first)
              (list a b)))
        result
        (->> attempts
             (filter #(= 19690720 (first %)))
             (first))]
    (+ (nth result 2) (* 100 (nth result 1)))))

(time (let [part1 (first (try-input 12 2))
      part2 (part2)]
  (println [part1 part2])
  (list part1 part2)))
;; "Elapsed time: 231.394075 msecs"
;; (5305097 4925)
