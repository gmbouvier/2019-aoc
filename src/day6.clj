(ns advent.day5
  (:require [clojure.java.io :as io]))

(def input
  (map #(map keyword (clojure.string/split % #"\)"))
       (-> "day6.raw" (io/resource) (io/reader) (line-seq))))

(def system-map
  (reduce
   (fn [system-map orbit]
     (conj system-map
           {(first orbit)
            (conj (or ((first orbit) system-map) []) (last orbit))}))
   {}
   input))

(defn invert-system-map [system-map]
  (reduce-kv
   (fn [inverted k v]
     (conj inverted (reduce #(conj %1 {%2 k}) {} v))) {} system-map))

(def inverted-system-map
  (invert-system-map system-map))

(defn all-orbits
  ([key] (indirect-orbits 0 key))
  ([level key]
   (if (get system-map key)
     (+ level (apply + (map (partial indirect-orbits (inc level)) (get system-map key))))
     level)))

(defn path-up-to-com
  ([key] (up-to-com [] key))
  ([path key]
   (if (= key :COM)
     path
     (recur
      (conj path (get inverted-system-map key))
      (get inverted-system-map key)))))

(let [first-shared-parent
      (->> (path-up-to-com :YOU)
           (map #(some #{%} (path-up-to-com :SAN)))
           (filter #(not (nil? %)))
           (first))
      part1 (all-orbits :COM)
      part2 (apply + (map #(.indexOf (up-to-com %) first-shared-parent)
                          [:YOU :SAN]))]
  (map println [part1 part2]))
