(ns advent.day5
  (:require [clojure.java.io :as io]))

(def input
  (into [] (map (fn [i] (Integer/parseInt i)))
        (clojure.string/split
         (-> "day5.raw" (io/resource) (io/reader) (line-seq) (first))
         #",")))

(defn arity [opcode]
  (get {1 3, 2 3, 3 1, 4 1, 5 2, 6 2, 7 3, 8 3, 99 0} opcode))

(defn argslist [opcode program pointer op-seq]
  (let [i-fn #(fn [] (->> % (+ pointer) (nth program)))
        p-fn #(fn [] (nth program ((i-fn %))))
        arg #(hash-map :i (i-fn %) :p (p-fn %))
        args #(->> % (inc) (range 1) (map arg))
        determine-mode #(conj %2 {:mode %1})
        modemap (->> op-seq (reverse) (drop 2)
                     (map #(if (= % \1) :i :p)))]
    (map determine-mode
         (concat modemap (repeat :p))
         (args (arity opcode)))))

(defn apply-op [opcode argslist program pointer id]
  (let [coerce-by-mode #(((:mode %) %))
        operands (map coerce-by-mode (take 2 argslist))
        next-pointer (+ pointer (inc (count argslist)))
        out #(assoc program ((:i (last argslist))) %)
        jump-op #(if (% (first operands)) (last operands) (+ 3 pointer))
        result #(conj {:program program :pointer next-pointer} %)]
    (cond
      (=  1 opcode) (result {:program (out (apply + operands))})
      (=  2 opcode) (result {:program (out (apply * operands))})
      (=  3 opcode) (result {:program (out id)})
      (=  4 opcode) (result {:print   (first operands)})
      (=  5 opcode) (result {:pointer (jump-op (comp not zero?))})
      (=  6 opcode) (result {:pointer (jump-op zero?)})
      (=  7 opcode) (result {:program (out (if (apply < operands) 1 0))})
      (=  8 opcode) (result {:program (out (if (apply = operands) 1 0))})
      (= 99 opcode) (result {:program :done}))))

(defn do-prog
  ([program id] (do-prog 0 program id))
  ([pointer program id]
   (let [op-seq (->> (nth program pointer) (str) (seq))
         opcode (->> op-seq (take-last 2) (apply str) (Integer/parseInt))
         argslist (argslist opcode program pointer op-seq)
         result (apply-op opcode argslist program pointer id)]
     (if (:print result) (println (:print result)))
     (if (not= (:program result) :done)
       (recur (:pointer result) (:program result) id)))))

(map #(do-prog input %) [1 5])
