(ns advent.day7
  (:require [clojure.java.io :as io]
            [clojure.math.combinatorics :as combo]))

(def input
  (into [] (map (fn [i] (Integer/parseInt i)))
        (clojure.string/split
         (-> "day7.raw" (io/resource) (io/reader) (line-seq) (first))
         #",")))

(def arity {1 3, 2 3, 3 1, 4 1, 5 2, 6 2, 7 3, 8 3, 99 0})

(defn modemap [op-seq]
  (concat
   (->> op-seq (reverse) (drop 2)
        (map #(if (= % \1) :i :p)))
   (repeat :p)))

(defn argslist [opcode state modemap]
  (let [{:keys [program pointer]} state
        i-fn #(fn [] (->> % (+ pointer) (nth program)))
        p-fn #(fn [] (nth program ((i-fn %))))
        args #(->> % (inc) (range 1)
                   (map (fn [n] {:i (i-fn n) :p (p-fn n)})))]
    (map (fn [mode arg] (conj arg {:m (mode arg)}))
         modemap
         (args (arity opcode)))))

(defn apply-op [state]
  (let [{:keys [program pointer output]} state
        op-seq (->> (nth program pointer) (str) (seq))
        opcode (->> op-seq (take-last 2) (apply str) (Integer/parseInt))
        argslist (argslist opcode state (modemap op-seq))
        operands (map #((:m %)) (take 2 argslist))
        next-pointer (+ pointer (inc (count argslist)))
        default-result {:program program :pointer next-pointer :output output}
        output-n (fn [n] (assoc program ((:i (last argslist))) n))
        jump-op (fn [p] (if (p (first operands)) (last operands) next-pointer))
        result #(conj default-result %)]
    (case opcode
      1  (result {:program (output-n (apply + operands))})
      2  (result {:program (output-n (apply * operands))})
      3  (result {:contfn #(result {:program (output-n %)})})
      4  (result {:output  (conj (or output []) (first operands))})
      5  (result {:pointer (jump-op (complement zero?))})
      6  (result {:pointer (jump-op zero?)})
      7  (result {:program (output-n (if (apply < operands) 1 0))})
      8  (result {:program (output-n (if (apply = operands) 1 0))})
      99 (result {:contfn  (fn [& args] :done)}))))

(defn next-cont-state [state]
  (->> (iterate apply-op state)
       (filter :contfn)
       (first)))

(defn provide-first-input [i]
  (next-cont-state
   ((-> {:program input :pointer 0}
        (next-cont-state)
        (:contfn)) i)))

(defn output-signal [settings]
  (let [amps (map provide-first-input settings)]
    (loop [amps amps input 0]
      (let [waiting-result ((:contfn (first amps)) input)]
        (if (= :done waiting-result)
          (last (map last (map :output amps)))
          (let [current-amp (next-cont-state waiting-result)]
            (recur (concat (rest amps) [current-amp])
                   (last (:output current-amp)))))))))

(let [part1 #{0 1 2 3 4} part2 #{5 6 7 8 9}]
  (map #(->> (combo/permutations %)
             (map output-signal)
             (apply max))
       [part1 part2]))
