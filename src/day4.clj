(ns advent.day4
  (:require [clojure.string :as str]))

(def input
  (map (fn [line] (map #(Character/digit % 10) line))
       (str/split
        (->> "src/day4.raw"
             (slurp)
             (drop-last 1)
             (apply str))
        #"-")))

(defn list->int [list]
  (Integer/parseInt (str/join (map str list))))

(defn positions [positions minmap maxmap]
  (let [add-positions
        (fn [branch]
          (map #(conj (vec branch) %)
               (range
                (max
                 (or (minmap (inc (count branch)))
                     0)
                 (last branch))
                (inc
                 (or (maxmap (inc (count branch)))
                     9)))))]
    (loop [result positions]
      (if (= (count (first result)) 6)
        result
        (recur (mapcat add-positions result))))))

(defn valid? [start end pos]
  (let [identity-partition (partition-by identity pos)
        any-duplicate?
        (not= (count pos)
              (count identity-partition))
        correct-duplicate?
        (->> identity-partition
             (map count)
             (some #(= 2 %)))]
    (and any-duplicate? correct-duplicate?)))

(let [start (first input) end (last input)
      first-possible
      (->> start
           (reduce #(conj %1 (max %2 (or (last %1) 0))) [])
           (zipmap (range 1 7)))
      full-positions
      (->> (first end)
           (range (inc (first start)))
           (map list))]
  (->> (positions full-positions {} {})
       (concat (positions `((~(first start))) first-possible {}))
       (filter (partial valid? start end))
       (count)))
