(ns advent.day1
  (:require [clojure.java.io :as io]))

(time (let [input-file "day1.raw"

      fuel-counter-upper
      (fn [payload]
        (-> payload
            (quot 3)
            (- 2)))
      better-fuel-counter-upper
      (fn [payload]
        (->> payload
             (iterate fuel-counter-upper)
             (drop 1)
             (take-while #(> % 0))
             (reduce + 0)))

      summed-result
      (fn [f]
        (let [input (line-seq (io/reader (io/resource input-file)))
              xf (comp (map #(Integer/parseInt %)) (map f))]
          (transduce xf + input)))
      answers [(summed-result fuel-counter-upper)
               (summed-result better-fuel-counter-upper)]]
  (map println answers)))
;; "Elapsed time: 2.97214 msecs"
;; 3152919
;; 4726527
