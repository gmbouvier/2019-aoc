(ns advent.day3
  (:require [clojure.java.io :as io]
            [clojure.core.match :refer [match]]
            [clojure.math.numeric-tower :refer [abs]]))

(defn parse-line [line]
  (->> (clojure.string/split line #",")
       (map #(hash-map :dir (keyword (str (first %)))
                       :len (-> (rest %)
                                (clojure.string/join)
                                (Integer/parseInt))))))

(defn line-with-dir [point instr fnx fny]
  (loop [instr instr results [{:x (fnx (:x point)) :y (fny (:y point))}]]
    (let [len (last instr) pointer (last results)]
      (if (= len 0)
        results
        (recur (list (first instr) (dec len))
               (conj results {:x (fnx (:x pointer))
                              :y (fny (:y pointer))}))))))

(defn direction [point instr fnx fny]
  (loop [instr instr
         results [{:x (fnx (:x point)) :y (fny (:y point))}]]
    (let [len (:len instr) pointer (last results)]
      (if (= len 1) results
        (recur (hash-map :dir instr :len (dec len))
               (conj results {:x (fnx (:x pointer))
                              :y (fny (:y pointer))}))))))

(defn line-segment [point instr]
  (let [d (partial direction point instr)]
  (match [(:dir instr)]
         [:R] (d inc identity)
         [:L] (d dec identity)
         [:U] (d identity inc)
         [:D] (d identity dec))))

(defn line-path [line]
  (loop [position {:x 0 :y 0} result [{:x 0 :y 0}] line line]
    (if (empty? line) result
        (let [positions (line-segment position (first line))]
          (recur
           (last positions)
           (concat result positions)
           (drop 1 line))))))

(defn get-intersections [lines]
  (->> (map #(hash-map :intersection % :manhattan (apply + (map abs (vals %))))
            (reduce clojure.set/intersection (map set lines)))
       (sort-by last)
       (drop 1)))

(defn intersection-distances [lines intersections]
  (map (fn [intersection]
         (map (fn [line] (.indexOf line (:intersection intersection)))
              lines))
             intersections))

(time
 (let [lines (map #(-> % (parse-line) (line-path))
                  (-> "day3.raw" (io/resource) (io/reader) (line-seq)))
       intersections (get-intersections lines)
       answers (list
                (:manhattan (first intersections))
                (->> (intersection-distances lines intersections)
                     (map (partial apply +))
                     (apply min)))]
   (map println answers)))
;; "Elapsed time: 2839.456226 msecs"
;; 293
;; 27306
