(ns advent.day9)

(def input
  (vec (map #(Integer/parseInt %)
            (-> "src/day9.raw" (slurp) (clojure.string/trim)
                (clojure.string/split #",")))))
(def arity {1 3, 2 3, 3 1, 4 1, 5 2, 6 2, 7 3, 8 3, 9 1, 99 0})
(def default-modes (repeat (apply max (vals arity)) \0))

(defn output-val [prog pos val]
  (let [size (count prog)
        prog (if (< pos size)
               prog
               (->> (repeat (- (inc pos) size) 0) (concat prog) (vec)))]
    (assoc prog pos val)))

(defn apply-op [state]
  (let [{:keys [program pointer output base]} state
        [op args] (->> (nth program pointer) (str) (seq) (reverse) (split-at 2))
        opcode (->> (reverse op) (apply str) (Integer/parseInt))
        imode #(+ pointer (inc %))
        pmode #(nth program (imode %))
        argmode #(case %2 \0 (pmode %1) \1 (imode %1) \2 (+ base (pmode %1)))
        modelist (take (arity opcode) (concat args default-modes))
        argslist (map-indexed argmode modelist)
        next-pointer (+ pointer (inc (arity opcode)))
        operands (map #(nth program %) (take 2 argslist))
        output-val (partial output-val program (last argslist))
        jump-op #(if (% (first operands)) (second operands) next-pointer)
        default-result (conj state {:pointer next-pointer})
        result #(conj default-result %)]
    (case opcode
      1  (result {:program (output-val (apply + operands))})
      2  (result {:program (output-val (apply * operands))})
      3  (result {:contfn #(result {:program (output-val %)})})
      4  (result {:output  (conj (or output []) (first operands))})
      5  (result {:pointer (jump-op (complement zero?))})
      6  (result {:pointer (jump-op zero?)})
      7  (result {:program (output-val (if (apply < operands) 1 0))})
      8  (result {:program (output-val (if (apply = operands) 1 0))})
      9  (result {:base    (+ base (first operands))})
      99 (result {:contfn  (constantly (result {:pointer (dec next-pointer)}))
                  :pointer (dec next-pointer)}))))

(defn next-cont-state [state]
  (first (filter :contfn (iterate apply-op state))))

(defn provide-first-input [program i]
  (next-cont-state
   ((-> {:program program :pointer 0 :base 0}
        (next-cont-state)
        (:contfn)) i)))

(time (println (map #(:output (provide-first-input input %)) [1 2])))
